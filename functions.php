<?php
/**
 * KC Urology functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kcurology
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kcurology_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on kcurology, use a find and replace
		* to change 'kcurology' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'kcurology', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'primary' => esc_html__( 'Primary', 'kcurology' ),
			'top' => esc_html__( 'Top', 'kcurology' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background', apply_filters(
			'kcurology_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);

	/**
	 * Add support for default block styles.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/#default-block-styles
	 */
	add_theme_support( 'wp-block-styles' );
	/**
	 * Add support for wide aligments.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/#wide-alignment
	 */
	add_theme_support( 'align-wide' );

	/**
	 * Add support for block color palettes.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/#block-color-palettes
	 */
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Dusty orange', 'kcurology' ),
			'slug'  => 'dusty-orange',
			'color' => '#ed8f5b',
		),
		array(
			'name'  => __( 'Dusty red', 'kcurology' ),
			'slug'  => 'dusty-red',
			'color' => '#e36d60',
		),
		array(
			'name'  => __( 'Dusty wine', 'kcurology' ),
			'slug'  => 'dusty-wine',
			'color' => '#9c4368',
		),
		array(
			'name'  => __( 'Dark sunset', 'kcurology' ),
			'slug'  => 'dark-sunset',
			'color' => '#33223b',
		),
		array(
			'name'  => __( 'Almost black', 'kcurology' ),
			'slug'  => 'almost-black',
			'color' => '#0a1c28',
		),
		array(
			'name'  => __( 'Dusty water', 'kcurology' ),
			'slug'  => 'dusty-water',
			'color' => '#41848f',
		),
		array(
			'name'  => __( 'Dusty sky', 'kcurology' ),
			'slug'  => 'dusty-sky',
			'color' => '#72a7a3',
		),
		array(
			'name'  => __( 'Dusty daylight', 'kcurology' ),
			'slug'  => 'dusty-daylight',
			'color' => '#97c0b7',
		),
		array(
			'name'  => __( 'Dusty sun', 'kcurology' ),
			'slug'  => 'dusty-sun',
			'color' => '#eee9d1',
		),
	) );

	/**
	 * Optional: Disable custom colors in block color palettes.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/
	 *
	 * add_theme_support( 'disable-custom-colors' );
	 */

	/**
	 * Add support for font sizes.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/#block-font-sizes
	 */
	add_theme_support( 'editor-font-sizes', array(
		array(
			'name'      => __( 'small', 'kcurology' ),
			'shortName' => __( 'S', 'kcurology' ),
			'size'      => 16,
			'slug'      => 'small',
		),
		array(
			'name'      => __( 'regular', 'kcurology' ),
			'shortName' => __( 'M', 'kcurology' ),
			'size'      => 20,
			'slug'      => 'regular',
		),
		array(
			'name'      => __( 'large', 'kcurology' ),
			'shortName' => __( 'L', 'kcurology' ),
			'size'      => 36,
			'slug'      => 'large',
		),
		array(
			'name'      => __( 'larger', 'kcurology' ),
			'shortName' => __( 'XL', 'kcurology' ),
			'size'      => 48,
			'slug'      => 'larger',
		),
	) );

	/**
	 * Optional: Add AMP support.
	 *
	 * Add built-in support for the AMP plugin and specific AMP features.
	 * Control how the plugin, when activated, impacts the theme.
	 *
	 * @link https://wordpress.org/plugins/amp/
	 */
	add_theme_support( 'amp', array(
		'comments_live_list' => true,
	) );

}
add_action( 'after_setup_theme', 'kcurology_setup' );

/**
 * Set the embed width in pixels, based on the theme's design and stylesheet.
 *
 * @param array $dimensions An array of embed width and height values in pixels (in that order).
 * @return array
 */
function kcurology_embed_dimensions( array $dimensions ) {
	$dimensions['width'] = 720;
	return $dimensions;
}
add_filter( 'embed_defaults', 'kcurology_embed_dimensions' );

/**
 * Register Google Fonts
 */
function kcurology_fonts_url() {
	$fonts_url = '';

	/**
	 * Translator: If EB Garamond does not support characters in your language, translate this to 'off'.
	 */
	$eb_garamond = esc_html_x( 'on', 'EB Garamond font: on or off', 'kcurology' );
	/**
	 * Translator: If Montserrat does not support characters in your language, translate this to 'off'.
	 */
	$montserrat = esc_html_x( 'on', 'Montserrat font: on or off', 'kcurology' );

	$font_families = array();

	if ( 'off' !== $eb_garamond ) {
		$font_families[] = 'EB Garamond:400,700';
	}

	if ( 'off' !== $montserrat ) {
		$font_families[] = 'Montserrat:200,200i,400,400i,700,700i';
	}

	if ( in_array( 'on', array( $eb_garamond, $montserrat ) ) ) {
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );

}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function kcurology_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'kcurology-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'kcurology_resource_hints', 10, 2 );

/**
 * Enqueue WordPress theme styles within Gutenberg.
 */
function kcurology_gutenberg_styles() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'kcurology-fonts', kcurology_fonts_url(), array(), null ); // phpcs:ignore WordPress.WP.EnqueuedResourceParameters.MissingVersion

	// Add Elegant Icons, used in the main stylesheet.
	wp_enqueue_style( 'kcurology-icons', get_theme_file_uri( '/elegant-icons/css/elegant-icons.min.css' ), array(), null );

	// Enqueue main stylesheet.
	wp_enqueue_style( 'kcurology-base-style', get_theme_file_uri( '/css/editor-styles.css' ), array(), '20180514' );
}
add_action( 'enqueue_block_editor_assets', 'kcurology_gutenberg_styles' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kcurology_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Header Notice', 'kcurology' ),
		'id'            => 'header-notice',
		'description'   => esc_html__( 'Add widgets here.', 'kcurology' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'kcurology' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'kcurology' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Top', 'kcurology' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'kcurology' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Bottom Primary', 'kcurology' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'kcurology' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Bottom Secondary', 'kcurology' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'kcurology' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'kcurology_widgets_init' );

/**
 * Enqueue styles.
 */
function kcurology_styles() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'kcurology-fonts', kcurology_fonts_url(), array(), null ); // phpcs:ignore WordPress.WP.EnqueuedResourceParameters.MissingVersion

	// Enqueue main stylesheet.
	wp_enqueue_style( 'kcurology-base-style', get_stylesheet_uri(), array(), '20180514' );

	// Add Elegant Icons, used in the main stylesheet.
	wp_enqueue_style( 'kcurology-ui', get_theme_file_uri( '/css/ui.css' ), array(), '20180514' );

	// Register component styles that are printed as needed.
	wp_register_style( 'kcurology-comments', get_theme_file_uri( '/css/comments.css' ), array(), '20180514' );
	wp_register_style( 'kcurology-content', get_theme_file_uri( '/css/content.css' ), array(), '20180514' );
	wp_register_style( 'kcurology-sidebar', get_theme_file_uri( '/css/sidebar.css' ), array(), '20180514' );
	wp_register_style( 'kcurology-widgets', get_theme_file_uri( '/css/widgets.css' ), array(), '20180514' );
	wp_register_style( 'kcurology-front-page', get_theme_file_uri( '/css/front-page.css' ), array(), '20180514' );
}
add_action( 'wp_enqueue_scripts', 'kcurology_styles' );

/**
 * Enqueue scripts.
 */
function kcurology_scripts() {

	// If the AMP plugin is active, return early.
	if ( kcurology_is_amp() ) {
		return;
	}

	// Enqueue the navigation script.
	wp_enqueue_script( 'kcurology-navigation', get_theme_file_uri( '/js/navigation.js' ), array(), '20180514', false );
	wp_script_add_data( 'kcurology-navigation', 'async', true );
	wp_localize_script( 'kcurology-navigation', 'kcurologyScreenReaderText', array(
		'expand'   => __( 'Expand child menu', 'kcurology' ),
		'collapse' => __( 'Collapse child menu', 'kcurology' ),
	));

	// Enqueue skip-link-focus script.
	wp_enqueue_script( 'kcurology-skip-link-focus-fix', get_theme_file_uri( '/js/skip-link-focus-fix.js' ), array(), '20180514', false );
	wp_script_add_data( 'kcurology-skip-link-focus-fix', 'defer', true );

	// Enqueue comment script on singular post/page views only.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'kcurology_scripts' );

/**
 * Custom responsive image sizes.
 */
require get_template_directory() . '/inc/image-sizes.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/pluggable/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Optional: Add theme support for lazyloading images.
 *
 * @link https://developers.google.com/web/fundamentals/performance/lazy-loading-guidance/images-and-video/
 */
require get_template_directory() . '/pluggable/lazyload/lazyload.php';

/**
 * Icon shortcode
 *
 */
add_shortcode( 'icon', function( $atts = array(), $content = null ) {
	extract( shortcode_atts(array(
		'icon' => '',
		'ei' => true,
	), $atts));
	$class_prefix = $ei === true ? 'ei ei' : 'icon icon-';
	return "<span class='$class_prefix-$icon'></span>";
} );

/**
 * Core values shortcode
 *
 */
add_shortcode( 'core_value', function( $atts = array(), $content = null ) {
	extract( shortcode_atts(array(
		'icon' => 'stewardship',
		'title' => 'Stewardship',
	), $atts));

	return "
		<div class='flex__item'>
			<h5 class='core-values__title'>
				" . do_shortcode( '[icon icon="' . $icon . '" ei=false]' ) . "
				<span class='name'>" . $title . "</span>
			</h5>
			<div class='content'>
				" . do_shortcode( $content ) . "
			</div>
		</div>";
});

add_shortcode( 'core_values', function( $atts = array(), $content = null ) {
	extract( shortcode_atts(array(
		'add_class' => 'alignfull',
		'title' => 'Core Values',
	), $atts));

	return "<section class='section-core-values $add_class'>
			<h2 style='text-align: center';>$title</h2>
			<div class='flex flex--5-col'>
				" . do_shortcode( $content ) . "
			</div>
			<div id='core-values-content' class='core-values-content'></div>
		</section>";
} );

/**
 * Core values shortcode
 *
 */
add_shortcode( 'mission_vision', function($atts = array(), $content = null) {
	extract( shortcode_atts(array(
		'add_class' => 'has-primary-background-color has-white-color',
		'mission_title' => 'Our Mission',
		'mission_text' => 'To provide solutions and quality to our customers in all we do.',
		'vision_title' => 'Our Vision',
		'vision_text' => 'To continually support market growth with a thriving culture of hiring, training and mentoring people who are.',
	), $atts));
	$output = "<section class='section-mission-vision $add_class'>
			<h4>$mission_title</h4>
			<p>$mission_text</p>
			<h4>$vision_title</h4>
			<p>$vision_text</p>
		</section>";

	return $output;
} );
