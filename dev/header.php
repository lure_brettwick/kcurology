<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wprig
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( ! wprig_is_amp() ) : ?>
		<script>document.documentElement.classList.remove("no-js");</script>
	<?php endif; ?>

	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_theme_file_uri( '/elegant-icons/css/elegant-icons.min.css' ); ?>" >
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary">
		<?php esc_html_e( 'Skip to content', 'wprig' ); ?>
	</a>

	<?php if ( is_active_sidebar( 'header-notice' ) ) : ?>
		<div class="site-notice">
			<?php dynamic_sidebar( 'header-notice' ); ?>
		</div>
	<?php endif; ?>

	<header id="masthead" class="site-header">

		<div class="site-top-menu">
			<?php if ( has_nav_menu('top') ) {
				wp_nav_menu(
					array(
						'theme_location' => 'top',
						'menu_id'        => 'top',
						'container'      => 'ul',
						'depth' => 1,
					)
				);
			} ?>
		</div>

		<div class="site-header__row">
			<div class="site-branding">
				<?php the_custom_logo(); ?>
			</div><!-- .site-branding -->

			<button class="menu-search">
				<span class="ei ei-icon_search"></span>
			</button>

			<?php get_search_form(); ?>

			<button class="menu-toggle" aria-label="<?php esc_attr_e( 'Open menu', 'wprig' ); ?>" aria-controls="primary-menu" aria-expanded="false"
				<?php if ( wprig_is_amp() ) : ?>
					on="tap:AMP.setState( { siteNavigationMenu: { expanded: ! siteNavigationMenu.expanded } } )"
					[aria-expanded]="siteNavigationMenu.expanded ? 'true' : 'false'"
				<?php endif; ?>
			>
				<span class="ei ei-icon_menu"></span>
			</button>
		</div>

		<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Main menu', 'wprig' ); ?>"
			<?php if ( wprig_is_amp() ) : ?>
				[class]=" siteNavigationMenu.expanded ? 'main-navigation toggled-on' : 'main-navigation' "
			<?php endif; ?>
		>
			<?php if ( wprig_is_amp() ) : ?>
				<amp-state id="siteNavigationMenu">
					<script type="application/json">
						{
							"expanded": false
						}
					</script>
				</amp-state>
			<?php endif; ?>

			<div class="primary-menu-container">
				<?php

				if ( has_nav_menu('primary') ) {
					wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'menu_id'        => 'primary',
							'container'      => 'ul',
						)
					);
				}

				?>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
