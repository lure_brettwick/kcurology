<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wprig
 */

?>

<footer id="colophon" class="site-footer">
	<div class="site-info">
		<aside id="footer-top" class="footer-top widget-area">
			<?php dynamic_sidebar( 'footer-1' ); ?>
		</aside><!-- #footer-top -->
		<aside id="footer-bottom-primary" class="footer-bottom-primary widget-area">
			<?php dynamic_sidebar( 'footer-2' ); ?>
		</aside><!-- #footer-bottom-primary -->
		<aside id="footer-bottom-secondary" class="footer-bottom-secondary widget-area">
			<?php dynamic_sidebar( 'footer-3' ); ?>
		</aside><!-- #footer-bottom-secondary -->
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
